﻿using UnityEngine;
using System.Collections;

public class GameSceneController : MonoBehaviour
{

    GameplayController gamePlayController;

    public GameObject FadeObject;

    public static bool needShowFade = false;

    void Start()
    {
        needShowFade = false;
        FadeObject.GetComponent<Animation>().Play("FadeIn");
        FadeObject.SetActive(true); //20191029: This to avoid of activating /deactivating this object every time I edit the scene...
        gamePlayController = GameObject.FindObjectOfType<GameplayController>();

        //20191030: ads initialization
        GoogleAds.Instance.InitializeBanner();
        GoogleAds.Instance.ShowBanner();
        GoogleAds.Instance.RequestInterstitial();

        // Initial
        // Levelselect switch
        LevelSelectController.NewUnlockedLevel = -1;
        LevelSelectController.needUpdateMoveSquirrelIcon = false;
    }

    void Update()
    {
        if (needShowFade)
        {
            needShowFade = false;
            FadeObject.GetComponent<Animation>().Play("FadeOut");
        }
    }

    // Scene Controlling
    // Menu Popup Area
    public GameObject PauseMenu, WinMenu, EndlessWinMenu, LoseMenu, OptionMenu;
    public UnityEngine.UI.Button PauseButton;
    public void PauseGame()
    {
        if (gamePlayController.gamePaused)
        {
            gamePlayController.gamePaused = false;
            PauseMenu.gameObject.SetActive(false);
            PauseButton.enabled = true;
        }
        else
        {
            gamePlayController.gamePaused = true;
            PauseMenu.gameObject.SetActive(true);
            PauseButton.enabled = false;
        }
    }

    public void OpenSetting()
    {
        PauseMenu.gameObject.SetActive(false);
        OptionMenu.gameObject.SetActive(true);
    }

    public void CloseSetting()
    {
        PauseMenu.gameObject.SetActive(true);
        OptionMenu.gameObject.SetActive(false);
    }

    public void RestartLevel()
    {
        GoogleAds.Instance.HideBanner(); //To hide banner from map (en ambos modos)
        GoogleAds.Instance.ShowInterstitial();
        
        Application.LoadLevel("GameScene");
    }

    //20191030: Atencion que en el modo Endless NO se reinicia la pantalla, luego, el banner no debería de ocultar.
    //En el caso de Puzzle mode, SI se oculta puesto que deseamos que el banner NO se muestre en el mapa del juego
    //Por otra parte, el evento Start vuelve a mostrar el banner (el evento Start no se dispara en endless mode).
    public void NextLevel()
    {
        
        GoogleAds.Instance.ShowInterstitial();
        if (GlobalData.gameMode == GlobalData.GameMode.PUZZLE_MODE)
        {
            GoogleAds.Instance.HideBanner(); //To hide banner from map
            LevelSelectController.ShowPlayDialog(GlobalData.GetCurrentLevel() + 1);
            GoToHomeScene();
        }
        else if (GlobalData.gameMode == GlobalData.GameMode.ENDLESS_MODE)
        {
            // Continue gameplay
            EndlessWinMenu.SetActive(false);

            gamePlayController.ContinuePlayEndless();
        }
    }

    public void GoToHomeScene()
    {
        GoogleAds.Instance.HideBanner(); //Ocultar el banner ya sea porque elegimos un nivel o porque volvemos al menu principal.

        GameSceneController.needShowFade = true;

        // Reset load counter
        BranchPairSetup.ResetAllValue();

        if (GlobalData.gameMode == GlobalData.GameMode.PUZZLE_MODE)
        {
            Application.LoadLevel("LevelSelect");
        }
        else if (GlobalData.gameMode == GlobalData.GameMode.ENDLESS_MODE)
        {
            Application.LoadLevel("HomeScene");
        }
    }

}
