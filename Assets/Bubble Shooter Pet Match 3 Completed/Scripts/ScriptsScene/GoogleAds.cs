using System;
using UnityEngine;
using GoogleMobileAds;
using GoogleMobileAds.Api;

/// <summary>
/// 20191030: Minimizo el c�digo que no tiene uso, salvo que quiera hacer tests
/// </summary>
public class GoogleAds : MonoBehaviour
{
    private BannerView bannerView;
    private InterstitialAd interstitial;

	public string Android_Banner_id;
	public string Android_Interstitial_id;

	public static GoogleAds Instance;

	void Start()
	{
		if(Instance==null)
		{	
			Instance=this;
			DontDestroyOnLoad(gameObject);
			Debug.Log("dontdestroy*********");	
			//RequestBanner();
			
		}
		else
		{
			Destroy(gameObject);
			Debug.Log("Destroy*******");
		}
		
	}

    //Importante: el evento Loaded dispara la visualizacion por lo que no hay un ShowBanner
    public void InitializeBanner()
    {
        #if UNITY_EDITOR
            string adUnitId = "unused";
        #elif UNITY_ANDROID
		string adUnitId = Android_Banner_id;
        #elif UNITY_IPHONE
            string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
        #else
            string adUnitId = "unexpected_platform";
        #endif

        // Create a 320x50 banner at the top of the screen.
        if (bannerView!=null)
        {
            bannerView.Destroy();
        }
        bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);
        // Register for ad events.
        bannerView.OnAdLoaded += HandleAdLoaded;
        bannerView.OnAdFailedToLoad += HandleAdFailedToLoad;
        //bannerView.OnAdOpening += HandleAdOpened;
        //bannerView.OnAdClosed += HandleAdClosing;
        //bannerView.OnAdLeavingApplication += HandleAdLeftApplication;
        // Load a banner ad.
        bannerView.LoadAd(createAdRequest());
    }

    public void ShowBanner()
    {
        bannerView.Show();
    }

    public void HideBanner()
    {
        bannerView.Hide();
    }

    public void RequestInterstitial()
    {
        #if UNITY_EDITOR
        string adUnitId = "unused";
        #elif UNITY_ANDROID
		string adUnitId = Android_Interstitial_id;
        #elif UNITY_IPHONE
            string adUnitId = "INSERT_IOS_INTERSTITIAL_AD_UNIT_ID_HERE";
        #else
            string adUnitId = "unexpected_platform";
        #endif
        if (interstitial!=null)
        {
            interstitial.Destroy();
        }
        // Create an interstitial.
        interstitial = new InterstitialAd(adUnitId);
        // Register for ad events. 
        //20191030: It has not sense creating some event without use. Minimizar o suprimir c�digo sin uso.

        interstitial.OnAdFailedToLoad += HandleInterstitialFailedToLoad;
        interstitial.OnAdClosed += HandleInterstitialClosed;
        // Load an interstitial ad.
        interstitial.LoadAd(createAdRequest());
    }

    private AdRequest createAdRequest()
    {
        return new AdRequest.Builder().Build();
    }

        public void ShowInterstitial()
    {
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }
        else
        {
            Debug.Log("Interstitial is not ready yet.");
        }
    }

    #region Banner callback handlers

    public void HandleAdLoaded(object sender, EventArgs args)
    {
        Debug.Log("HandleAdLoaded event received.");
		bannerView.Show();
    }

    public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.Log("HandleFailedToReceiveAd event received with message: " + args.Message);
        InitializeBanner();
    }
    #endregion

    
    #region Interstitial callback handlers


    public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.Log("HandleInterstitialFailedToLoad event received with message: " + args.Message);
        RequestInterstitial();
    }

    public void HandleInterstitialClosed(object sender, EventArgs args)
    {
        Debug.Log("HandleInterstitialClosed event received");
        RequestInterstitial();
    }
    #endregion
}


/*
    private AdRequest createAdRequest()
    {
        return new AdRequest.Builder().Build();

        .AddTestDevice(AdRequest.TestDeviceSimulator)
                .AddTestDevice("0123456789ABCDEF0123456789ABCDEF")
                .AddKeyword("game")
                .SetGender(Gender.Male)
                .SetBirthday(new DateTime(1985, 1, 1))
                .TagForChildDirectedTreatment(false)
                .AddExtra("color_bg", "9B30FF")
                .Build();
    }
    */

#region test
/*  void OnGUI()
  {
      // Puts some basic buttons onto the screen.
      GUI.skin.button.fontSize = (int) (0.05f * Screen.height);

      Rect requestBannerRect = new Rect(0.1f * Screen.width, 0.05f * Screen.height,
                                 0.8f * Screen.width, 0.1f * Screen.height);
      if (GUI.Button(requestBannerRect, "Request Banner"))
      {
          RequestBanner();
      }

      Rect showBannerRect = new Rect(0.1f * Screen.width, 0.175f * Screen.height,
                                     0.8f * Screen.width, 0.1f * Screen.height);
      if (GUI.Button(showBannerRect, "Show Banner"))
      {
          bannerView.Show();
      }

      Rect hideBannerRect = new Rect(0.1f * Screen.width, 0.3f * Screen.height,
                                     0.8f * Screen.width, 0.1f * Screen.height);
      if (GUI.Button(hideBannerRect, "Hide Banner"))
      {
          bannerView.Hide();
      }

      Rect destroyBannerRect = new Rect(0.1f * Screen.width, 0.425f * Screen.height,
                                        0.8f * Screen.width, 0.1f * Screen.height);
      if (GUI.Button(destroyBannerRect, "Destroy Banner"))
      {
          bannerView.Destroy();
      }

      Rect requestInterstitialRect = new Rect(0.1f * Screen.width, 0.55f * Screen.height,
                                              0.8f * Screen.width, 0.1f * Screen.height);
      if (GUI.Button(requestInterstitialRect, "Request Interstitial"))
      {
          RequestInterstitial();
      }

      Rect showInterstitialRect = new Rect(0.1f * Screen.width, 0.675f * Screen.height,
                                           0.8f * Screen.width, 0.1f * Screen.height);
      if (GUI.Button(showInterstitialRect, "Show Interstitial"))
      {
          ShowInterstitial();
      }

      Rect destroyInterstitialRect = new Rect(0.1f * Screen.width, 0.8f * Screen.height,
                                              0.8f * Screen.width, 0.1f * Screen.height);
      if (GUI.Button(destroyInterstitialRect, "Destroy Interstitial"))
      {
          interstitial.Destroy();
      }
  }
*/
#endregion
