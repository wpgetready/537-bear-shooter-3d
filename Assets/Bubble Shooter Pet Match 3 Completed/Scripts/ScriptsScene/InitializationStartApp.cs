﻿using UnityEngine;
using System.Collections;
using StartApp;

public class InitializationStartApp : MonoBehaviour {

	// Use this for initialization
	void Start () {
		#if UNITY_ANDROID
		StartAppWrapper.init();
		#endif	
	}

	void OnGUI () {
		#if UNITY_ANDROID
		StartAppWrapper.showSplash();
		#endif
	}
	

}
