Bubble Shooter Pet Match 3 Completed
Complete project for your match3/bubble shooter game. 

Requires Unity 4.6 or higher.
Game's Features:
- This is a completed game designer, sound, effects, gameplay, full map, full resource, full animation, for such a full sound.

- Support you until you feel the most satisfaction for this asset.

- Support multi-platform: Android, iOs, Window Phone 8, Web Player, ...

- Support for both Unity 4.6.x and Unity 5.x

- Complete package

- Animated GUIs by Unity 4.6

- Multiple screen sizing for all mobile devices.

- Level selector

- Amazing system maps: Full 120 attractive difference levels.

- Easy customize for all platform

- There are 10 kinds of super beautiful ball with different colors. There are various ball as an obstacle. (Ice ball, iron ball, ...)

- Rich items system: Bomb ball, ball destroy all same color balls, ...

- Settings menu

For more detail, please click on Demo link or download and install APK file to play

All of your comments about the product, please send an email to vietgamestudio@gmail.com

If you have any concerns or questions about this product, please contact with us immediately. We will support you as soon as possible.

NOTE: it is template project have 120 example levels with 2 types of gameplay with the same design. Please change design of levels to your own ideas.


WHAT's IN THE BUBBLE SHOOTER PET MATCH 3 COMPLETED ?
----------------------------

The Bubble Shooter Pet Match 3 Completed contains quite a lot of stuff. 
Everything is in the Assets folder, and in it you'll find the following folders :

- Animation : contains all the animations in the game

- Audio : sound and music used for game

- Fonts : Custom bitmap fonts

- Graphics: all the sprites and sprite sheets used in the game. Feel free to reuse them in your own game.

- Physics : Bounce physic for balls

- Perfabs: all the game's prefabs. You just have to drag one of these in your scene and they'll be ready (bubble, frost, levelbutton, other..)

- Resources : contain map data, note and configuration for game.

- Scenes: scenes in the game.( note : choose home scene in Scenes folder to start game)

- Scripts: all the game scripts. This includes CustomComponents, EditorAddon, Gameplay, Managers, SceneScript, Utilities,...

WHAT AM I SUPPOSED TO DO WITH IT ?
----------------------------------

The Bubble Shooter Pet Match 3 Completed erything you need to start your game.
You can either start fresh and pick the scripts you need from it, or modify the existing demo levels brick by brick, adding your own sprites and scripts as you go.

USAGE
-------------------------------
When Imported project --> File --> Build Setting (or Ctrl+ Shirt + B) --> Check scenes in build exists HomeScene, LevelSelect, GameScene. If not exists click button "Add Current" to add scene or drag all scenes in folder Scenes of Project to Scene In Build window.

NOTE
-------------------------------
- Choose HomeScene scene in Scenes folder to start game

- When Import project into unity. If exist bug : "UnityException: Tag: flyingbubble is not defined!" you can fix :

Select Edit menu --> Project Settings --> Tags and Layers. In Inspector select Tags & Layers add two tag : bubble and flyingbubble.



EMAIL SUPPORT
--------------------------------
thongvantran45@gmail.com or vietgamestudio@gmail.com

VERSION
--------------------------------

1.0 : First release
